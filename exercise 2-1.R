devtools::install_github("arunsrinivasan/flights")
downloadflightlogs (year = 2014L, month = 1:12, path = "./", dir = "flights", 
                    verbose = TRUE)
flights (year = 2014L, month = 1:12, path = "./", dir = "flights", 
         select = NULL, verbose = TRUE) 
input <- if (file.exists("flights14.csv")) {
  "flights14.csv"
} else {
  "https://raw.githubusercontent.com/Rdatatable/data.table/master/vignettes/flights14.csv"
}
flights<- fread(input)
flights

dim(flights)

DT = data.table(
  ID = c("b","b","b","a","a","c"),
  a = 1:6,
  b = 7:12,
  c = 13:18
)
DT
class(DT$ID)

ans <- flights[origin== "JFK" & month == 6L]
ans

flights[1:2]

flights[order(origin, -dest)]

head(flights[, arr_delay])
head(flights[,list(arr_delay)])
head(flights[, .(delay_arr=arr_delay, delay_dep=dep_delay)])

flights[, sum( (arr_delay + dep_delay) < 0)]
flights[origin == "JFK" & month == 6L, .(m_arr = mean(arr_delay), m_dep = mean(dep_delay))]
flights[origin == "JFK" & month == 6L, length(dest)]
flights[origin == "JFK" & month == 6L, .N]
head(flights[, c("arr_delay","dep_delay")])

cols = c("arr_delay","dep_delay")
head(flights[,..cols])
head(flights[, cols, with = FALSE])

df= data.frame(x=c(1,1,1,2,2,3,3,3), y = 1:8)
df[df$x >1 ,]
df[with(df,x > 1), ]

flights[origin == "JFK" & month == 6L, .N]
flights[, .(.N), by = .(origin)]
flights[, .N, by = origin]
flights[carrier == "AA", .N, by = origin]
flights[carrier == "AA", .N, by = .(origin,dest)]
flights[carrier == "AA", .(mean(arr_delay),mean(dep_delay)), by = .(origin,dest,month)]
flights[carrier == "AA", .(mean(arr_delay),mean(dep_delay)), keyby = .(origin,dest,month)]
ans <- flights[carrier =="AA", .N, by = .(origin,dest)]
head(ans[order(origin, -dest)])

head(flights[carrier =="AA", .N, by = .(origin,dest)][order(origin,-dest)])
flights[, .N, .(dep_delay>0, arr_delay>0)]

DT
DT[, print(.SD), by = ID]
DT[, lapply(.SD, mean), by = ID]
flights[carrier == "AA", lapply(.SD, mean), by = .(origin, dest,month), .SDcols=c("arr_delay", "dep_delay")]
flights[, head(.SD, 2), by = month]

DT[, .(val = c(a,b)), by = ID]
DT[, .(val = list(c(a,b))), by = ID]
 