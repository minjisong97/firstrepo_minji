phoneCheck <- function(data){
  Phone= data[,phone]
  
  #clean the phone numbers
  clean.phone=gsub("[- .)(+]|[a-zA-Z]*:?","", Phone)
  digit <- data.table(nchar(clean.phone))
  
  #check if the cleaned data has 10 digits.
  phone_digit <- digit[, clean.phone:=ifelse(digit == 10, "TRUE", "FALSE")]
  
  #check if all of digits of the cleaned data didn't repeated.
  check0=as.list(str_extract(clean.phone, "0{10}"))
  check1=as.list(str_extract(clean.phone, "1{10}"))
  check2=as.list(str_extract(clean.phone, "2{10}"))
  check3=as.list(str_extract(clean.phone, "3{10}"))
  check4=as.list(str_extract(clean.phone, "4{10}"))
  check5=as.list(str_extract(clean.phone, "5{10}"))
  check6=as.list(str_extract(clean.phone, "6{10}"))
  check7=as.list(str_extract(clean.phone, "7{10}"))
  check8=as.list(str_extract(clean.phone, "8{10}"))
  check9=as.list(str_extract(clean.phone, "9{10}"))
  
  check_list= setDT(list(check0,check1,check2,check3,check4,check5,check6,check7,check8,check9))

  
  list<-check_list[rowSums(is.na(check_list)) != ncol(check_list),]
  return(list)
}


