version$version.string
RStudio.Version()$version



install.packages(c("ggplot2", "gapminder"))



library(ggplot2)
library(gapminder)


gapminder


ggplot(data=gapminder, mapping = aes(x = gdpPercap, y = lifeExp))+geom_point()


ggplot(data = gapminder, aes(x = gdpPercap, y = lifeExp, size = pop, col = continent)) + 
  geom_point(alpha = 0.3)



p=ggplot(data = gapminder, aes(x = gdpPercap, y = lifeExp))
p + geom_point(aes(size = pop, col = continent), alpha = 0.3) + geom_smooth(method = "loess")




ggplot(data = gapminder, aes(x = gdpPercap, y = lifeExp, size = pop, col = continent)) +
  geom_point(alpha = 0.3)  +
  geom_smooth(method = "loess")



ggplot(data = gapminder) + geom_density(aes(x = gdpPercap, fill = continent), alpha = 0.3)



p2 =
  p + 
  geom_point(aes(size = pop, col = continent), alpha = 0.3) +
  scale_color_brewer(name = "Continent", palette = "Set1") + ## Different colour scale
  scale_x_log10(labels = scales::dollar) + ## Switch to logarithmic scale on x-axis. Use dollar units.
  scale_size(name = "Population", labels = scales::comma) +
  labs(x = "Log (GDP per capita)", y = "Life Expectancy") +
  theme_bw()

p2



if (!require("pacman")) install.packages("pacman")

pacman::p_load(hrbrthemes, gganimate)
library(gganimate)



ggplot(gapminder, aes(gdpPercap, lifeExp, size = pop, Color = country)) +
  geom_point(alpha = 0.7, show.legend = FALSE) +
  scale_color_manual(values = country_colors) +
  scale_size(range = c(2,12)) +
  scale_x_log10() +
  facet_wrap(~continent) +
  labs(title = 'Year: {frame_time}', x = 'GDP per Capita', y= 'life expectancy') + transition_time(year) + ease_aes('linear')



